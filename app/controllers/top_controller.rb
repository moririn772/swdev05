class TopController < ApplicationController
  def main
    if session[:uid].nil?
      render "main"
    else
      render "login"
    end
  end

  def login
    user = User.find_by(uid: user_params[:uid])
    return redirect_to top_error_path if user.nil?

    if User.authenticate?(user.pass, user_params[:pass])
      session[:uid] ||= user.uid
      redirect_to top_main_path
    else
      redirect_to top_error_path
    end
  end

  def logout
    session.delete(:uid)
    redirect_to root_path
  end

  def error
  end

  private

  def user_params
    params.permit(:uid, :pass)
  end
end
