class UsersController < ApplicationController
  def index
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    @user.pass = BCrypt::Password.create(user_params[:pass])
    if @user.save
      redirect_to root_path
    else
      render new_user_path
    end
  end

  private

  def user_params
    params.require(:user).permit(:uid, :pass)
  end
end
